package com.zuitt.wdc044.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String username;

    @Column
    private String password;

    //represents the one side of relationship
    @OneToMany(mappedBy="user")
    //prevent infinite recursion with bidirectional relationships
    @JsonIgnore

    //a collection in java that cannot contain any duplicate elements
    private Set<Post> posts;

    public User(){}

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    //This will be used for the JWT implementation
    public Long getId(){
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    //a getter function for getting posts
    public Set<Post> getPosts(){
        return posts;
    }

}
