package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.User;

import java.util.Optional;

// This interface will be used to register a user via UserController.
public interface UserService {
    // register a user functionality
    void createUser(User user);

    // Check username functionality
    // Optional-container object that can be a nullable object
    // This will check if the user already exists before proceeding with the registration.
    // This is also will be used on authenticating the user.
    Optional<User> findByUsername(String username);

}
