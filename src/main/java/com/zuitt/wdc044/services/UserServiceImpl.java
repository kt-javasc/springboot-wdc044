package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

// "@Service" Annotation is used to indicate that it holds the actual business logic.
@Service
public class UserServiceImpl implements UserService{
    // "@Autowired" is used to access objects and methods of another class.
    @Autowired
    private UserRepository userRepository;

    //CrudRepository Methods Summary: https://docs.spring.io/spring-data/commons/docs/current/api/org/springframework/data/repository/CrudRepository.html

    // Create a user
    public void createUser(User user){
        userRepository.save(user);
    }

    // Checking if user exists
    // returns the entity of given criteria or
    // an empty instance of the optional class
    public Optional<User> findByUsername(String username){
        return Optional.ofNullable(userRepository.findByUsername(username));
    }
}
