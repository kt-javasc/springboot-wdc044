package com.zuitt.wdc044.services;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.PostRepository;
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImpl implements PostService{

    @Autowired
    private PostRepository postRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
     JwtToken jwtToken;

    // creating a post
    public void createPost(String stringToken, Post post) {
        // We use the findByUsername method to find the user.
        // We can retrieve the username in the "payload" of the jwt.
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        System.out.println(author);
        Post newPost = new Post();
        //  Title and content will come from the req.body which is pass through the "post" identifier.
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());
        // author will come from the decoded jwt payload.
        newPost.setUser(author);
        postRepository.save(newPost);
    }

    //getting all posts
    public Iterable<Post> getPosts() {
        return postRepository.findAll();
    }

    // edit post
    public ResponseEntity updatePost(Long id, String stringToken, Post post) {
        // We will retrieve the post for updating using the "id" provided.
        Post postForUpdating = postRepository.findById(id).get();
        // Because relationship is establish within the User and Post model, we are able to retrieve the username of the owner of the post.
        String postAuthor = postForUpdating.getUser().getUsername();
        // We will also retrieved the username of the current login user for checking.
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        // This will check if the currently logged in user is the owner of the post.
        // If the username of the authenticated user matches the username of the post owner, it will excute the update
        if(authenticatedUser.equals(postAuthor)) {
            postForUpdating.setTitle(post.getTitle());
            postForUpdating.setContent(post.getContent());
            postRepository.save(postForUpdating);
            return new ResponseEntity<>("Post updated successfully", HttpStatus.OK);
        }
        // It will send an UNAUTHORIZED response if the username didn't match.
        else {
            return new ResponseEntity<>("You are not authorized to edit this post.", HttpStatus.UNAUTHORIZED);
        }
    }

    // delete post
    public ResponseEntity deletePost(Long id, String stringToken) {
        Post postForUpdating = postRepository.findById(id).get();
        String postAuthor= postForUpdating.getUser().getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);
        if(authenticatedUser.equals(postAuthor)) {
            postRepository.deleteById(id);
            return new ResponseEntity<>("Post deleted successfully", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You are not authorized to edit this post.", HttpStatus.UNAUTHORIZED);
        }

    }


}
