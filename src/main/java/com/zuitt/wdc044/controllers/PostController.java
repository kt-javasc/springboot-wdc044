package com.zuitt.wdc044.controllers;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
//enable cross origin requests via @CrossOrigin
@CrossOrigin
public class PostController {
    @Autowired
    PostService postService;

    //create a new post
    @RequestMapping(value="/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post){

        // "ResponseEntity" represents the whole HTTP response: status code, headers, and body
        //We can access the "postService" methods and pass the following arguments:
        // stringToken of the current session will be retrieve in the request headers.
        // a "post" object will be instantiated upon receiving the request body, this will follow the properties set in the model.
        // note: the "key" name of the request from postman should be similar to the property names set in the model.

        postService.createPost(stringToken, post);
        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }

    // Get all posts
    @RequestMapping(value="/posts", method = RequestMethod.GET)
    public ResponseEntity<Object> getPosts() {
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }

    // Edit a user post
    @RequestMapping(value="/posts/{postid}", method = RequestMethod.PUT)
    // @PathVariables is used for data passed in URI
    // @RequestParam vs @PathVariables
    // @RequestParam is used to extract data found in query parameters (commonly used for filtering the results based on the condition)
    // @PathVariables is used to retrieve exact record.
    public ResponseEntity<Object> updatePost(@PathVariable Long postid, @RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post) {

        // This will call the updatePost method in the postService
        return postService.updatePost(postid, stringToken, post);
    }

    // delete a user post
    @RequestMapping(value = "/posts/{postid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable Long postid, @RequestHeader(value = "Authorization") String stringToken) {
        return postService.deletePost(postid, stringToken);
    }

}